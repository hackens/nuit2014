#!/usr/bin/env python2
#-*- coding: utf-8 -*-

from animation import set_color, fade, direct, close
from time import sleep
from random import choice, shuffle

led = range(26)

rouge = '#ff0000'
orange = '#ff8c00'
jaune = '#ffff00'
vert_pomme = '#72ff15'
vert = '#00ff00'
bleu_turquoise = '#15ffd5'
bleu_clair = '#15a4ff'
bleu = '#0000ff'
violet = '#ae15ff'
rose = '#ff43b6'
blanc = '#ffffff'
noir = '#000000'


def anim_shuffle(colors, duration=3.0):
	shuffle(colors)
	for i in range(len(colors)):
		set_color(led[i::len(colors)], colors[i])
	fade(duration)


def anim_arrow(color, max_led=len(led), duration=0.05):
	for i in range(max_led):
		set_color([i], color)
		fade(duration)



def anim1():
	set_color(led[::2], orange)
	set_color(led[1::2], noir)
	fade(3)

	set_color(led[::2], rouge)
	set_color(led[1::2], orange)
	fade(1)

	set_color(led, jaune)
	fade(0.5)


def anim2():
	"""To yellow blink"""
	for i in range(20):
		l = choice(led)
		set_color([l], blanc)
		direct()
		set_color([l], jaune)
		direct()

def anim3():
	"""Pomme d'api"""
	for i in led:
		set_color([i], vert_pomme if i % 2 == 0 else vert)
		fade(0.2)


def anim4():
	"""spéciale dédikasse Machin"""
	for i in range(len(led)/2):
		set_color([i], vert)
		set_color([len(led) - 1 - i], rouge)
		fade(0.05)

	for i in range(len(led)/2):
		set_color([len(led)/2 - i, len(led)/2 + i], orange)
		fade(0.05)


def anim5():
	"""Bleu/Rouge"""
	set_color(led, bleu)
	direct()
	sleep(0.1)
	set_color(led, rouge)
	direct()
	sleep(0.1)


def anim6():
	"""Rouge/Vert/Bleu shuffle"""
	anim_shuffle([rouge, vert, bleu], 1)


def anim7():
	"""Violet/Jeune/Orange shuffle"""
	anim_shuffle([violet, jaune, orange])


def anim8():
	anim_arrow(rouge)
	anim_arrow(jaune)


def anim9():
	"""Coup de bleu"""
	set_color(led, noir)
	direct()
	sleep(0.5)
	set_color(led, bleu_clair)
	direct()
	set_color(led, noir)
	direct()
	set_color(led, bleu_clair)
	direct()
	sleep(1.5)

def anim10():
	for i in range(10):
		l = choice(led)
		set_color([l], choice([bleu, blanc, rouge, rouge]))
		direct()

def anim11():
	set_color(led, rouge)

def anim12():
	anim9()
	for i in range(10):
		anim10()
	anim11()


def main():
	anims = [anim1, anim2, anim3, anim4, anim5, anim6, anim7, anim8, anim9,
                 anim10, anim11, anim12]
	while True:
		choice(anims)()


if __name__ == '__main__':
	try:
		while True:
			main()
	finally:
		close()
