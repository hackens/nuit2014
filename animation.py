#!/usr/bin/env python2

import control

serial_port = "/dev/ttyUSB0"
nb_leds = 26

brightness = 1.0
corrections = {'r': 0.863, 'g': 1.0, 'b': 0.863}
animation = []

control = control.Control(serial_port,
                          nb_leds,
                          brightness=brightness,
                          corrections=corrections)

leds_colors = [{'r': 0, 'g': 0, 'b': 0} for i in range(nb_leds)]


def hex2dict(color):
    color = color.strip("# ")
    color = int(color, 16)
    B = color & 0xFF
    G = (color & 0xFF00) >> 8
    R = (color & 0xFF0000) >> 16
    return {'r': R, 'g': G, 'b': B}


def prepare_data():
    data = {}
    for led in range(len(leds_colors)):
        data[led] = leds_colors[led]
    return data


def set_color(led_list, hex_color):
    global leds_colors
    for i in led_list:
        if i >= nb_leds or i < 0:
            continue
        leds_colors[i] = hex2dict(hex_color)


def fade(duration):
    control.send_colors(prepare_data(), fading=True, fading_duration=duration)


def direct():
    control.send_colors(prepare_data(), fading=False)


def close():
    control.close()
