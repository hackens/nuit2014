#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# Needs the full sketch on the LEDs
# Will blink each LED in Red, Green and Blue

# Usage : ./test_serial.py SERIAL_PORT start end
# start and end are numbers for the first and last LED to test

# If the Uno resets when launching the program, try to run
# stty -hup -F SERIAL_PORT

# -----------------------------------------------------------------------------
# "THE NO-ALCOHOL BEER-WARE LICENSE" (Revision 42):
# Phyks (webmaster@phyks.me) wrote or updated these files for hackEns. As long
# as you retain this notice you can do whatever you want with this stuff
# (and you can also do whatever you want with this stuff without retaining it,
# but that's not cool...).
#
# If we meet some day, and you think this stuff is worth it, you can buy us a
# <del>beer</del> soda in return.
#                                                       elarnon for hackEns
# -----------------------------------------------------------------------------

import serial
import sys
import os

def send_color(ser, start, end, r, g, b):
    for k in range(start, end):
        for j in [0x80+k, int(r / 2), int(g / 2), int(b / 2)]:
            ser.write(chr(j))

def main(ser, start, end, col):
    b = col & 0xff
    g = (col >> 8) & 0xff
    r = (col >> 16) & 0xff
    send_color(ser, start, end, r, g, b)

if __name__ == '__main__':
    if(len(sys.argv) < 5):
        sys.exit("Usage : "+sys.argv[0]+" SERIAL_PORT start end 0xRRGGBB")

    try:
        ser = serial.Serial(sys.argv[1], 115200)
        start = int(sys.argv[2])
        end = int(sys.argv[3])
        col = int(sys.argv[4][2:], 16)
    except Exception as e:
        print(e)
        sys.exit("Unable to open serial port.")

    try:
        main(ser, start, end, col)
    finally:
        ser.close()
